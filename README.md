# Tired Fizzbuzz

A sleep-based Fizz Buzz implementation in the spirit of [sleep sort](https://rosettacode.org/wiki/Sorting_algorithms/Sleep_sort).

Crédits to Mara Bos:

- Site: [m-ou.se](https://m-ou.se)
- Blog: [blog.m-ou.se](https://blog.m-ou.se)
- E-mail: [m-ou.se@m-ou.se](https://twitter.com/m_ou_se/)
- Twitter: @m_ou_se
- Github: [github.com/m-ou-se](https://github.com/m-ou-se)
- Crates.io: [crates.io/users/m-ou-se](https:://crates.io/users/m-ou-se)

## Usage ?

None ! Just run ``` cargo run ``` and behold  the results:

![Tired Fizz Buzz results](static/images/result.png)
