use std::{thread::{self, sleep}, time::Duration};

//
// A wait-based Fizz Buzz implementation in the spirit of "sleep sort" https://rosettacode.org/wiki/Sorting_algorithms/Sleep_sort.
// 
// Crédits Mara Bos 
//     Site: m-ou.se
//     Blog: blog.m-ou.se
//     E-mail: m-ou.se@m-ou.se
//     Twitter: @m_ou_se
//     Github: github.com/m-ou-se
//     Crates.io: crates.io/users/m-ou-se
//
fn main() {
    thread::spawn(|| {
        sleep(Duration::from_millis(2100));
        loop {
            print!("Fizz");
            sleep(Duration::from_secs(3));
        }
    });
    thread::spawn(|| {
        sleep(Duration::from_millis(4200));
        loop {
            print!("Buzz");
            sleep(Duration::from_secs(5));
        }
    });
    
    for i in 1.. {
        print!("\n{}\r",i);
        sleep(Duration::from_secs(1));
    }
}
